package demo

import (
	"fmt"
	"goapp/internal/pkg/config"
)

func printEnv(cfg *config.Configuration) {
	fmt.Println("env is", cfg.Env)
}