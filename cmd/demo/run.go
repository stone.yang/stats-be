package demo

import (
	"github.com/spf13/cobra"
	"goapp/internal/pkg/config"
)

var Cmd = &cobra.Command{
	Use:   "demo",
	Short: "",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		config.EnvPrefix = "goapp"
		cfg := config.New("app.yml")
		printEnv(cfg)
	},
}
