package scheduler

import (
	"goapp/cmd/dataanalysis"
	"goapp/internal/pkg/config"

	log "github.com/sirupsen/logrus"
)

func initialize(cfg *config.Configuration) (err error) {
	config.EnvPrefix = "goapp"
	err = dataanalysis.Initialize(cfg)
	if err != nil {
		return err
	}
	return nil
}

func syncDataJob() {
	err := dataanalysis.ReportService.SyncData()
	if err != nil {
		log.Panicf("sumService.SyncData: %v", err)
		return
	}
}
