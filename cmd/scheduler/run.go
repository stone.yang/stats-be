package scheduler

import (
	"fmt"
	"goapp/internal/pkg/config"
	"goapp/internal/pkg/db"
	"runtime"
	"time"

	"github.com/robfig/cron/v3"

	log "github.com/sirupsen/logrus"

	"github.com/spf13/cobra"
)

var Cmd = &cobra.Command{
	Use:   "jobs",
	Short: "",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		defer func() {
			if r := recover(); r != nil {
				// unknown error
				err, ok := r.(error)
				if !ok {
					err = fmt.Errorf("unknown error: %v", r)
				}
				trace := make([]byte, 4096)
				runtime.Stack(trace, true)
				log.WithField("stack_trace", string(trace)).WithError(err).Panic("unknown error")
			}
		}()
		// 配合現行DB, 用utc時區存local時間
		db.NowFunc = func() time.Time {
			format := "2006-01-02 15:04:05"
			timeString := time.Now().Format(format)
			now, _ := time.ParseInLocation(format, timeString, time.UTC)
			return now
		}

		config.EnvPrefix = "goapp"
		cfg := config.New("app.yml")
		err := initialize(cfg)
		if err != nil {
			log.Panicf("main: initialize failed: %v", err)
			return
		}

		//syncDataJob()
		c := cron.New()
		c.AddFunc(cfg.SchedulerCfg.MoneyStatsFreq, syncDataJob)
		c.Start()

		defer c.Stop()
		select {}
	},
}
