package dataanalysis

import (
	"goapp/internal/pkg/config"
	"goapp/internal/pkg/db"
	"goapp/pkg/dataanalysis"
	databases "goapp/pkg/dataanalysis/repository/database"
	services "goapp/pkg/dataanalysis/service"

	"github.com/labstack/echo/v4/middleware"

	"github.com/labstack/echo/v4"

	"net/http"

	log "github.com/sirupsen/logrus"

	delivery "goapp/pkg/dataanalysis/delivery/http"

	docs "goapp/cmd/dataanalysis/docs"

	echoSwagger "github.com/swaggo/echo-swagger"
)

var (
	ReportService dataanalysis.ReportService
)

func Initialize(cfg *config.Configuration) (err error) {
	readDB, writeDB, err := db.InitDatabases(cfg.DatabasesCfg, "dataanalysis")
	if err != nil {
		return err
	}
	log.Info("main: DB initialized")
	// repo
	moneyStatsRepo := databases.NewMoneyStatsRepository(writeDB)
	reportRepo := databases.NewReportRepository(readDB, writeDB)
	// service
	ReportService = services.NewReportService(moneyStatsRepo, reportRepo)
	return nil
}

// @title Swagger Example API
// @version 1.0
// @description This is a sample server Petstore server.

// @contact.name API Support
// @contact.url http://www.swagger.io/support

// @host petstore.swagger.io
// @BasePath /v2
func newEchoHandler(cfg *config.Configuration) http.Handler {
	if err := Initialize(cfg); err != nil {
		log.Panicf("main: initialize failed: %v", err)
		return nil
	}
	docs.SwaggerInfo.Host = cfg.APIServerCfg.AdvertiseAddr
	docs.SwaggerInfo.BasePath = "/api"
	//echo.NotFoundHandler = errors.NotFoundHandlerForEcho
	//echo.MethodNotAllowedHandler = errors.NotFoundHandlerForEcho
	e := echo.New()
	e.Debug = false
	e.HideBanner = true
	e.HidePort = true
	//e.HTTPErrorHandler = errors.HTTPErrorHandlerForEcho
	handler := delivery.NewHandler(ReportService)

	e.GET("/ping", func(c echo.Context) error {
		return c.String(http.StatusOK, "pong!!!")
	})

	e.Use(middleware.CORS())

	e.GET("/swagger/*", echoSwagger.WrapHandler)

	delivery.SetRoutes(e, handler)
	return e
}
