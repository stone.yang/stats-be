package stats

import (
	"goapp/internal/pkg/config"
	"goapp/internal/pkg/db"
	"goapp/internal/pkg/timeutil"
	"goapp/pkg/stats"
	statsDatabase "goapp/pkg/stats/repository/database"
	statsService "goapp/pkg/stats/service"

	log "github.com/sirupsen/logrus"
)

var (
	sumService stats.SumService
)

func initialize(cfg *config.Configuration) (err error) {
	readDB, writeDB, err := db.InitDatabases(cfg.DatabasesCfg, "stats")
	if err != nil {
		return err
	}
	log.Info("main: DB initialized")

	// repo
	table1TestRepo := statsDatabase.NewTestTable1Repository(readDB, writeDB)
	table2TestRepo := statsDatabase.NewTestTable2Repository(readDB, writeDB)

	// service
	sumService = statsService.NewSumService(table1TestRepo, table2TestRepo)

	return nil
}

func run() {
	start, err := timeutil.StringParseTime("2020-03-16 00:00:00")
	if err != nil {
		log.Panicf("timeutil.StringParseTime failed: %v", err)
		return
	}
	end, err := timeutil.StringParseTime("2020-03-16 00:43:00")
	if err != nil {
		log.Panicf("timeutil.StringParseTime failed: %v", err)
		return
	}
	err = sumService.Sum0(start, end)
	if err != nil {
		log.Panicf("sumService.Sum0 failed: %v", err)
		return
	}
}

//goland:noinspection GoUnusedFunction
func run2() {
	start, err := timeutil.StringParseTime("2020-03-16 00:00:00")
	if err != nil {
		log.Panicf("timeutil.StringParseTime failed: %v", err)
		return
	}
	end, err := timeutil.StringParseTime("2020-03-16 00:43:00")
	if err != nil {
		log.Panicf("timeutil.StringParseTime failed: %v", err)
		return
	}
	err = sumService.Sum2(start, end)
	if err != nil {
		log.Panicf("sumService.Sum1 failed: %v", err)
		return
	}
}
