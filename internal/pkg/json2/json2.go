package json2

import (
	jsoniter "github.com/json-iterator/go"
)

var json = jsoniter.Config{
	EscapeHTML:             true,
	SortMapKeys:            true,
	ValidateJsonRawMessage: true,
	TagKey:                 "json2",
}.Froze()

// Marshal with tag `json2`
func Marshal(v interface{}) ([]byte, error) {
	return json.Marshal(v)
}

// Unmarshal with tag `json2`
func Unmarshal(data []byte, v interface{}) error {
	return json.Unmarshal(data, v)
}
