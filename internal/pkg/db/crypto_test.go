package db_test

import (
	"encoding/json"
	"goapp/internal/pkg/db"
	"os"
	"testing"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func TestCryptoGorm(t *testing.T) {
	db.EncryptoKey = []byte("123412324234")
	type Product struct {
		Code       string         `gorm:"column:code"`
		Name       db.Crypto      `gorm:"column:name"`
		NoteImages db.StringArray `gorm:"column:note_images;type:BLOB;"`
	}
	writeDB, err := gorm.Open(sqlite.Open("test.db"), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}
	defer func() {
		sqlDB, _ := writeDB.DB()
		sqlDB.Close()
	}()
	writeDB.Debug()
	//writeDB.LogMode(true)

	// Migrate the schema
	writeDB.AutoMigrate(&Product{})

	p := Product{}
	err = json.Unmarshal([]byte(`{"code":"L1212","name":"測試名"}`), &p)
	if err != nil {
		t.Error(err)
	}
	// Create
	err = writeDB.Create(&p).Error
	if err != nil {
		t.Error(err)
	}

	// Read
	var product Product
	// writeDB.First(&product, 1)                   // find product with id 1
	err = writeDB.First(&product, "code = ?", "L1212").Error // find product with code l1212
	if err != nil {
		t.Error(err)
	}
	t.Logf("%#v , length:%v", product, len(product.NoteImages))

	// Update - update product's price to 2000
	err = writeDB.Model(&product).Update("note_images", db.StringArray{"img3"}).Error
	if err != nil {
		t.Error(err)
	}
	// writeDB.First(&product, 1)                   // find product with id 1
	err = writeDB.First(&product, "name = ?", db.Crypto("測試名")).Error // find product with code l1212
	if err != nil {
		t.Error(err)
	}
	t.Logf("%#v , length:%v", product, len(product.NoteImages))

	os.Remove("test.db")
}
