package db

import (
	"gorm.io/gorm"
)

type queryMode int8

const (
	queryModeForUpdate queryMode = iota + 1
	queryModeLockInShareMode
)

// SQLBuilder don't call it
// example:
// db.Where("id = ?", 123).Where("name = ?", "name")
type SQLBuilder struct {
	whereConds []whereCond
	omit       []string
	only       map[string]bool
	order      []interface{}

	queryMode queryMode
}

// MergeSQLBuilder merge SQLBuilder into one
func MergeSQLBuilder(builders ...*SQLBuilder) *SQLBuilder {
	ret := newSQLBuilder()
	for i := range builders {
		b := builders[i]
		if b != nil {
			for j := range b.whereConds {
				whereCond := b.whereConds[j]
				ret.Where(whereCond.Query, whereCond.Args...)
			}
			ret.Omit(b.omit...)
			for j := range b.only {
				column := j
				ret.Only(column)
			}
			ret.order = append(ret.order, b.order...)
		}
	}
	return ret
}

func newSQLBuilder() *SQLBuilder {
	return &SQLBuilder{
		only: make(map[string]bool),
	}
}

// HasWhere check builder has where condition
func (s *SQLBuilder) HasWhere() bool {
	return len(s.whereConds) > 0
}

// Apply apply sql
func (s *SQLBuilder) Apply(session *gorm.DB, bean interface{}) *gorm.DB {
	if len(s.only) > 0 {
		m := ToMap(bean)
		for column := range m {
			if _, ok := s.only[column]; !ok {
				s.omit = append(s.omit, column)
			}
		}
	}
	if len(s.omit) > 0 {
		session = session.Omit(s.omit...)
	}
	for i := range s.whereConds {
		cond := s.whereConds[i]
		session = session.Where(cond.Query, cond.Args...)
	}
	for i := range s.order {
		orderby := s.order[i]
		session = session.Order(orderby)
	}
	switch s.queryMode {
	case queryModeForUpdate:
		session = session.Set("gorm:query_option", "FOR UPDATE")
	case queryModeLockInShareMode:
		session = session.Set("gorm:query_option", "LOCK IN SHARE MODE")
	}
	return session
}

// ForUpdate need call db.Apply before select
func ForUpdate() *SQLBuilder {
	s := newSQLBuilder()
	s.queryMode = queryModeForUpdate
	return s
}

// ForUpdate need call db.Apply before select
func (s *SQLBuilder) ForUpdate(order interface{}) *SQLBuilder {
	s.queryMode = queryModeForUpdate
	return s
}

// LockInShareMode need call db.Apply before select
func LockInShareMode() *SQLBuilder {
	s := newSQLBuilder()
	s.queryMode = queryModeLockInShareMode
	return s
}

// LockInShareMode need call db.Apply before select
func (s *SQLBuilder) LockInShareMode(order interface{}) *SQLBuilder {
	s.queryMode = queryModeLockInShareMode
	return s
}

// Order specify order when retrieve records from database, set reorder to `true` to overwrite defined conditions
//     db.Order("name DESC")
//     db.Order(gorm.Expr("name = ? DESC", "first")) // sql expression
func Order(order interface{}) *SQLBuilder {
	s := newSQLBuilder()
	s.order = append(s.order, order)
	return s
}

// Order specify order when retrieve records from database, set reorder to `true` to overwrite defined conditions
//     db.Order("name DESC")
//     db.Order(gorm.Expr("name = ? DESC", "first")) // sql expression
func (s *SQLBuilder) Order(order interface{}) *SQLBuilder {
	s.order = append(s.order, order)
	return s
}

// Omit for create/update column
func Omit(omit ...string) *SQLBuilder {
	s := newSQLBuilder()
	s.omit = omit
	return s
}

// Omit for create/update column
func (s *SQLBuilder) Omit(omit ...string) *SQLBuilder {
	s.omit = append(s.omit, omit...)
	return s
}

// Only for create/update column
func Only(columns ...string) *SQLBuilder {
	s := newSQLBuilder()
	for i := range columns {
		s.only[columns[i]] = true
	}
	return s
}

// Only for create/update column
func (s *SQLBuilder) Only(columns ...string) *SQLBuilder {
	for i := range columns {
		s.only[columns[i]] = true
	}
	return s
}

// Where is where helper
// example:
// db.Where("id = ?", 123).Where("name = ?", "name")
func Where(query interface{}, args ...interface{}) *SQLBuilder {
	s := newSQLBuilder()
	s.whereConds = []whereCond{
		whereCond{query, args},
	}
	return s
}

// Where is where helper
// example:
// db.Where("id = ?", 123).Where("name = ?", "name")
func (s *SQLBuilder) Where(query interface{}, args ...interface{}) *SQLBuilder {
	s.whereConds = append(s.whereConds, whereCond{
		Query: query,
		Args:  args,
	})
	return s
}

type whereCond struct {
	Query interface{}
	Args  []interface{}
}
