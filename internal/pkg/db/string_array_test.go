package db_test

import (
	"encoding/json"
	"goapp/internal/pkg/db"
	"os"
	"testing"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

func TestStringArray_UnmarshalJSON(t *testing.T) {
	type fields struct {
		NoteImages db.StringArray `json:"noteImages"`
	}
	type args struct {
		data []byte
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
		{"empty list", fields{}, args{[]byte(`{"noteImages":[]}`)}, false},
		{"list of string", fields{}, args{[]byte(`{"noteImages":["img1","img2"]}`)}, false},
		{"null", fields{}, args{[]byte(`{"noteImages":null}`)}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := json.Unmarshal(tt.args.data, &tt.fields); (err != nil) != tt.wantErr {
				t.Errorf("db.StringArray.UnmarshalJSON() error = %v, wantErr %v", err, tt.wantErr)
			}
			b, err := json.Marshal(tt.fields)
			if err != nil {
				t.Errorf("db.StringArray.MarshalJSON() error = %v, wantErr %v", err, tt.wantErr)
			}
			t.Logf(string(b))
		})
	}
}

func TestStringArrayGorm(t *testing.T) {
	type Product struct {
		Code       string         `gorm:"column:code"`
		NoteImages db.StringArray `gorm:"column:note_images;type:BLOB;"`
	}
	writeDB, err := gorm.Open("sqlite3", "test.db")
	if err != nil {
		panic("failed to connect database")
	}
	defer writeDB.Close()

	// Migrate the schema
	writeDB.AutoMigrate(&Product{})

	p := Product{}
	err = json.Unmarshal([]byte(`{"code":"L1212"}`), &p)
	if err != nil {
		t.Error(err)
	}
	// Create
	err = writeDB.Create(&p).Error
	if err != nil {
		t.Error(err)
	}

	// Read
	var product Product
	// writeDB.First(&product, 1)                   // find product with id 1
	err = writeDB.First(&product, "code = ?", "L1212").Error // find product with code l1212
	if err != nil {
		t.Error(err)
	}
	t.Logf("%#v , length:%v", product, len(product.NoteImages))

	// Update - update product's price to 2000
	err = writeDB.Model(&product).Update("note_images", db.StringArray{"img3"}).Error
	if err != nil {
		t.Error(err)
	}
	// writeDB.First(&product, 1)                   // find product with id 1
	err = writeDB.First(&product, "code = ?", "L1212").Error // find product with code l1212
	if err != nil {
		t.Error(err)
	}
	t.Logf("%#v , length:%v", product, len(product.NoteImages))

	os.Remove("test.db")
}
