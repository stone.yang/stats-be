package timeutil

import (
	"fmt"
	"time"
)

// TimezoneOffset UTC+0時區相對的偏移
type TimezoneOffset int32

// GetTimeOfUserTimeZone 取得依timezoneOffset換算過的現在時間
func (t *TimezoneOffset) GetTimeOfUserTimeZone(opts ...string) time.Time {
	zoneName := "UserTimezone"
	if len(opts) > 0 && opts[0] != "" {
		zoneName = opts[0]
	}
	userTimezone := time.FixedZone(zoneName, int((time.Duration(int(*t)) * time.Hour).Seconds()))
	now := time.Now().In(userTimezone)
	return now
}

// GetTimezoneOffset 取得實際的timezoneOffset
func (t *TimezoneOffset) GetTimezoneOffset() string {
	return fmt.Sprintf("+%02d:00", int32(*t))
}
