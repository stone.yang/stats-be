package timeutil

import (
	"fmt"
	"reflect"
	"testing"
	"time"
)

func TestIsZero(t *testing.T) {
	type args struct {
		t time.Time
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "test system zero time",
			args: args{
				t: time.Unix(0, 0),
			},
			want: true,
		},
		{
			name: "test time zero",
			args: args{
				t: time.Time{},
			},
			want: true,
		},
		{
			name: "not zero time (1)",
			args: args{
				t: time.Unix(1, 0),
			},
			want: false,
		},
		{
			name: "not zero time (2)",
			args: args{
				t: time.Now(),
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsZero(tt.args.t); got != tt.want {
				t.Errorf("IsZero() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStringToTime(t *testing.T) {
	defaultFormat := "2006-01-02T15:04:05.000Z"
	testFormat1 := ""
	testTimeStr1 := "1989-10-16T00:03:05.000Z"
	expectedTime1, expectedTime1Err := time.Parse(defaultFormat, testTimeStr1)

	testFormat2 := defaultFormat
	testTimeStr2 := "1994-11-04T00:03:05.000Z"
	expectedTime2, expectedTime2Err := time.Parse(testFormat2, testTimeStr2)

	testFormat3 := "2006-01-02 15:04"
	testTimeStr3 := "1953-04-01 09:28"
	expectedTime3, expectedTime3Err := time.Parse(testFormat3, testTimeStr3)

	sysZeroTime := time.Unix(0, 0)
	testFormat4 := "wrong format"
	testTimeStr4 := "1989-10-16T00:03:05.000Z"
	_, expectedTime4Err := time.Parse(testFormat4, testTimeStr4)
	expectedTime4 := sysZeroTime

	testFormat5 := defaultFormat
	testTimeStr5 := "wrong time string"
	_, expectedTime5Err := time.Parse(testFormat5, testTimeStr5)
	expectedTime5 := sysZeroTime

	type args struct {
		strTime string
		format  string
	}
	tests := []struct {
		name          string
		args          args
		want          time.Time
		wantErr       bool
		expectedError error
	}{
		{
			name: "not format,will use default format 2006-01-02T15:04:05.000Z",
			args: args{
				strTime: testTimeStr1,
				format:  testFormat1,
			},
			want:          expectedTime1,
			wantErr:       false,
			expectedError: expectedTime1Err,
		},
		{
			name: `format with "2006-01-02T15:04:05.000Z"`,
			args: args{
				strTime: testTimeStr2,
				format:  testFormat2,
			},
			want:          expectedTime2,
			wantErr:       false,
			expectedError: expectedTime2Err,
		},
		{
			name: `format with "2006-01-02 15:04"`,
			args: args{
				strTime: testTimeStr3,
				format:  testFormat3,
			},
			want:          expectedTime3,
			wantErr:       false,
			expectedError: expectedTime3Err,
		},
		{
			name: `wrong format,will return system zero time"`,
			args: args{
				strTime: testTimeStr4,
				format:  testFormat4,
			},
			want:          expectedTime4,
			wantErr:       true,
			expectedError: expectedTime4Err,
		},
		{
			name: `wrong time string,will return system zero time"`,
			args: args{
				strTime: testTimeStr5,
				format:  testFormat5,
			},
			want:          expectedTime5,
			wantErr:       true,
			expectedError: expectedTime5Err,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := StringToTime(tt.args.strTime, tt.args.format)
			if (err == nil) == tt.wantErr {
				t.Errorf("StringToTime() error = '%v', wantErr '%v', expectedError '%v'", err, tt.wantErr, tt.expectedError.Error())
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("StringToTime() = '%v', want '%v'", got, tt.want)
			}
		})
	}
}

func TestString(t *testing.T) {
	type args struct {
		t      time.Time
		format string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "system zero time,with format ''",
			args: args{
				t:      time.Unix(0, 0).UTC(),
				format: "",
			},
			want: "1970-01-01T00:00:00.000Z",
		},
		{
			name: "system zero time,with format '2006-01-02T15:04:05.000Z'",
			args: args{
				t:      time.Unix(0, 0).UTC(),
				format: "2006-01-02T15:04:05.000Z",
			},
			want: "1970-01-01T00:00:00.000Z",
		},
		{
			name: "system zero time,with format '2006-01-02 15:04'",
			args: args{
				t:      time.Unix(0, 0).UTC(),
				format: "2006-01-02 15:04",
			},
			want: "1970-01-01 00:00",
		},
		{
			name: "system zero time,with wrong format",
			args: args{
				t:      time.Unix(0, 0).UTC(),
				format: "it a wrong format",
			},
			want: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := String(tt.args.t, tt.args.format); got != tt.want {
				t.Errorf("String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSimpleString(t *testing.T) {
	type args struct {
		t time.Time
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "time zone",
			args: args{
				t: time.Time{}.UTC(),
			},
			want: "",
		},
		{
			name: "time zone",
			args: args{
				t: time.Unix(0, 0).UTC(),
			},
			want: "",
		},
		{
			name: "1970-01-01 08:00",
			args: args{
				t: time.Unix(0, 1).UTC(),
			},
			want: "1970-01-01 00:00",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := SimpleString(tt.args.t); got != tt.want {
				t.Errorf("SimpleString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIsYear(t *testing.T) {
	tests := []struct {
		value interface{}
		want  bool
	}{
		{value: "01970", want: true},
		{value: -0, want: false},
		{value: "1970", want: true},
		{value: int(1971), want: true},
		{value: int32(1972), want: true},
		{value: int64(1973), want: true},
		{value: int(1974), want: true},
		{value: int32(1975), want: true},
		{value: int64(1976), want: true},
		{value: "0", want: false},
		{value: "0.1970", want: false},
		{value: -1970, want: false},
		{value: -0.1, want: false},
	}
	for _, tt := range tests {
		t.Run(fmt.Sprintf("%v", tt.value), func(t *testing.T) {
			year := ToYear(tt.value)
			if got := IsYear(year); got != tt.want {
				t.Errorf("IsYear() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIsMonth(t *testing.T) {
	tests := []struct {
		value interface{}
		want  bool
	}{
		{value: "01", want: true},
		{value: "2", want: true},
		{value: int(3), want: true},
		{value: int32(4), want: true},
		{value: int64(5), want: true},
		{value: uint(6), want: true},
		{value: uint32(7), want: true},
		{value: uint64(8), want: true},
		{value: "0", want: false},
		{value: "00", want: false},
		{value: "13", want: false},
		{value: "-1", want: false},
		{value: -1, want: false},
		{value: 13, want: false},
		{value: 0.01, want: false},
	}
	for _, tt := range tests {
		t.Run(fmt.Sprintf("%v", tt.value), func(t *testing.T) {
			month := ToMonth(tt.value)
			if got := IsMonth(month); got != tt.want {
				t.Errorf("IsMonth() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIsDay(t *testing.T) {
	tests := []struct {
		value interface{}
		want  bool
	}{
		{value: "01", want: true},
		{value: "2", want: true},
		{value: int(3), want: true},
		{value: int32(4), want: true},
		{value: int64(5), want: true},
		{value: uint(6), want: true},
		{value: uint32(7), want: true},
		{value: uint64(8), want: true},
		{value: "30", want: true},
		{value: 31, want: true},
		{value: "0", want: false},
		{value: "00", want: false},
		{value: "-1", want: false},
		{value: -1, want: false},
		{value: 0.01, want: false},
	}
	for _, tt := range tests {
		t.Run(fmt.Sprintf("%v", tt.value), func(t *testing.T) {
			day := ToDay(tt.value)
			if got := IsDay(day); got != tt.want {
				t.Errorf("IsDay() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIsDate(t *testing.T) {
	type args struct {
		y interface{}
		m interface{}
		d interface{}
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{args: args{y: "2019", m: "02", d: "28"}, want: true},
		{args: args{y: 1911, m: 1, d: 1}, want: true},
		{args: args{y: "2019", m: "02", d: "29"}, want: false},
		{args: args{y: 1979, m: 04, d: 31}, want: false},
		{args: args{y: 1979, m: 04, d: 31}, want: false},
	}
	for _, tt := range tests {
		t.Run(fmt.Sprintf("%v-%v-%v", tt.args.y, tt.args.m, tt.args.d), func(t *testing.T) {
			if got := IsDate(tt.args.y, tt.args.m, tt.args.d); got != tt.want {
				t.Errorf("IsDate() = %v, want %v", got, tt.want)
			}
		})
	}
}
