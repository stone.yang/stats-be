package timeutil

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

const (
	// AllowTimeFormat 系統統一使用的時間格式
	AllowTimeFormat = "2006-01-02T15:04:05.000Z"

	// ReportTimeFormat 報表用的時間格式
	ReportTimeFormat = "20060102150405"

	// TimeFormat is general time format
	TimeFormat = "2006-01-02 15:04:05"

	// SimpleTimeFormat is a simple time format without seconds
	SimpleTimeFormat = "2006-01-02 15:04"
)

var (
	// SysZeroTime 系統定義的零時 1970-01-01
	SysZeroTime = time.Unix(0, 0)

	// TimezoneBeijing is the timezone for Beijing
	TimezoneBeijing = time.FixedZone("Beijing Time", int((8 * time.Hour).Seconds()))
)

// IsZero 是否為zerotime
func IsZero(t time.Time) bool {
	if t.Equal(SysZeroTime) {
		return true
	}

	if t.IsZero() {
		return true
	}
	return false
}

// StringParseTime 2006-01-02 15:04:05
func StringParseTime(strTime string) (time.Time, error) {
	t, err := time.Parse(TimeFormat, strTime)
	if err != nil {
		t = SysZeroTime
	}
	return t, err
}

// StringToTime 字串轉時間 如果字串不符合指定格式，回傳SysZeroTime
func StringToTime(strTime string, format string) (time.Time, error) {
	if format == "" {
		format = AllowTimeFormat
	}

	t, err := time.Parse(format, strTime)
	if err != nil {
		t = SysZeroTime
	}
	return t, err
}

// StringToTimeInLocation 字串轉時間 如果字串不符合指定格式，回傳SysZeroTime
func StringToTimeInLocation(strTime string, format string, location *time.Location) (time.Time, error) {
	if format == "" {
		format = AllowTimeFormat
	}

	t, err := time.ParseInLocation(format, strTime, location)
	if err != nil {
		t = SysZeroTime
	}
	return t, err
}

// String 將傳入的時間轉換為字串，當format為非時間格式或轉換失敗時，回傳空字串
func String(t time.Time, format string) string {
	if format == "" {
		format = AllowTimeFormat
	}

	timeFormat := t.Format(format)
	t, err := time.Parse(format, timeFormat)
	if t.Year() == 0 || err != nil {
		return ""
	}

	return timeFormat
}

// SimpleString is a wrapper function
func SimpleString(t time.Time) string {
	if IsZero(t) {
		return ""
	}
	return String(t, SimpleTimeFormat)
}

// ReportString is a wrapper function
func ReportString(t time.Time) string {
	if IsZero(t) {
		return ""
	}
	return String(t, ReportTimeFormat)
}

// IsYear 檢查傳入的參數是否為年份
func IsYear(year int) bool {
	if year <= 0 {
		return false
	}
	return true
}

// ToYear 將參數轉為對應的年份以int傳回 錯誤時回0
func ToYear(y interface{}) int {
	var year int
	switch v := y.(type) {
	case string:
		value, err := strconv.Atoi(v)
		if err != nil {
			return 0
		}
		year = value
	case int:
		year = v
	case int32:
		year = int(v)
	case int64:
		year = int(v)
	case uint:
		year = int(v)
	case uint32:
		year = int(v)
	case uint64:
		year = int(v)
	}
	return year
}

// IsMonth 檢查傳入的參數是否為月份
func IsMonth(month int) bool {
	tMonth := time.Month(month)
	if tMonth < time.January || tMonth > time.December {
		return false
	}
	return true
}

// ToMonth 將參數轉為對應的月份以int傳回 錯誤時回0
func ToMonth(m interface{}) int {
	var month int
	switch v := m.(type) {
	case string:
		if len(v) > 2 { //only accept 1,2,3....12 or 01,02,03...12
			return 0
		}
		value, err := strconv.Atoi(v)
		if err != nil {
			return 0
		}
		month = value
	case int:
		month = v
	case int32:
		month = int(v)
	case int64:
		month = int(v)
	case uint:
		month = int(v)
	case uint32:
		month = int(v)
	case uint64:
		month = int(v)
	default:
		return 0
	}

	return month
}

// IsDay 檢查傳入的參數是否為日
func IsDay(day int) bool {
	if day < 1 || day > 31 {
		return false
	}
	return true
}

// ToDay 將參數轉為對應的月份以int傳回 錯誤時回0 目前只驗1~31
func ToDay(d interface{}) int {
	var day int
	switch v := d.(type) {
	case string:
		if len(v) > 2 { //only accept 1,2,3....12 or 01,02,03...12
			return 0
		}
		value, err := strconv.Atoi(v)
		if err != nil {
			return 0
		}
		day = value
	case int:
		day = v
	case int32:
		day = int(v)
	case int64:
		day = int(v)
	case uint:
		day = int(v)
	case uint32:
		day = int(v)
	case uint64:
		day = int(v)
	default:
		return 0
	}
	return day
}

// IsDate 是否為允許的日期
func IsDate(y, m, d interface{}) bool {
	year := ToYear(y)
	month := ToMonth(m)
	day := ToDay(d)
	var strTime strings.Builder

	_, _ = strTime.WriteString(fmt.Sprintf("%v-", year))
	if month < 10 {
		_, _ = strTime.WriteString("0")
	}
	_, _ = strTime.WriteString(fmt.Sprintf("%v-", month))
	if day < 10 {
		_, _ = strTime.WriteString("0")
	}
	_, _ = strTime.WriteString(fmt.Sprintf("%v", day))

	_, err := StringToTime(strTime.String(), "2006-01-02")
	if err != nil {
		return false
	}
	return true
}

// GetDayOfStartTimeAndEndTime 取得指定日期的開始時間與結束時間
func GetDayOfStartTimeAndEndTime(theDay time.Time, loc *time.Location) (startTime, endTime time.Time) {
	now := time.Now().In(loc)

	startTime = time.Date(theDay.Year(), theDay.Month(), theDay.Day(), 0, 0, 0, 0, loc).In(time.UTC)
	endTime = time.Date(theDay.Year(), theDay.Month(), theDay.Day(), 0, 0, 0, 0, loc).AddDate(0, 0, 1).In(time.UTC)

	if now.Year() == theDay.Year() &&
		now.Month() == theDay.Month() &&
		now.Day() == theDay.Day() {
		// 同一天, 就查詢到目前時間為止
		startTime = time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, loc).In(time.UTC)
		endTime = time.Date(now.Year(), now.Month(), now.Day(), now.Hour(), now.Minute(), now.Second(), now.Nanosecond(), loc).In(time.UTC)
	}
	return
}
