package dataanalysis

import (
	"github.com/shopspring/decimal"
)

type Step int8

const (
	_ Step = iota
	StepLessOrEqual2
	StepEqual3
	StepGreaterOrEqual3
)

//type MoneyStatsRow map[int]map[int]struct {
//	count int
//	money decimal.Decimal
//}

type MoneyStatsValue struct {
	Count int32           `json:"count"`
	Money decimal.Decimal `json:"money"`
}

type MoneyStatsQuery struct {
	Company   string          `json:"company"`
	TypeID    int32           `json:"typeID"`
	MoneyFlow string          `json:"moneyFlow"`
	Action    string          `json:"action"`
	Count     int32           `json:"count"`
	Money     decimal.Decimal `json:"money"`
}
