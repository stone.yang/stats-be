package database

import (
	"fmt"
	"goapp/internal/pkg/db"
	"goapp/pkg/dataanalysis"
	"time"

	"gorm.io/gorm"
)

func NewReportRepository(readDB, writeDB *gorm.DB) dataanalysis.ReportRepository {
	return &ReportRepo{
		readDB,
		writeDB,
	}
}

type ReportRepo struct {
	readDB  *gorm.DB
	writeDB *gorm.DB
}

func (repo *ReportRepo) GetSumData(start, end time.Time) (*dataanalysis.MoneyStats, error) {
	var result dataanalysis.MoneyStats
	err := repo.readDB.Table((&dataanalysis.MoneyStats{}).TableName()).
		Select("sum(money1) as total1, sum(money2) as total2").
		Where("date_time BETWEEN ? AND ?", start, end).
		First(&result).Error
	return &result, err
}

func (repo *ReportRepo) BetJoinWorldCupEventStatus(start, end time.Time, typeID, statusID int, betStatus string) ([]dataanalysis.MoneyStats, error) {
	session := repo.readDB.Table("so_credit.bet a").
		Select("a.l80, SUM(a.dealmoney) as total_money, count(1) as total_count, ? as type_id", typeID).
		Joins("LEFT JOIN so_credit.world_cup_event_status b on a.eventid = b.game_id AND a.marketId = b.market_id AND a.selectionId = b.selection_id").
		Where(`
a.betStatus=? AND a.errorCode!=1
 AND a.createtime between ? AND ?
 AND IFNULL(b.status_id,0) = ?
`, betStatus, start, end, statusID).
		Group("a.l80")
	list := []dataanalysis.MoneyStats{}
	err := session.Find(&list).Error
	return list, err
}

// only 10102
func (repo *ReportRepo) Bet(start, end time.Time, typeID int) ([]dataanalysis.MoneyStats, error) {
	session := repo.readDB.Table("so_credit.bet a").
		Select("a.l80, SUM(a.dealmoney) as total_money,count(1) as total_count, ? as type_id", 10102).
		Where(`
a.betStatus='M' AND a.errorCode=1
 AND a.createtime between ? AND ?
`, start, end).
		Group("a.l80")
	list := []dataanalysis.MoneyStats{}
	err := session.Find(&list).Error
	return list, err
}

func (repo *ReportRepo) AccountingJoinWorldCupEventStatus(start, end, openDate time.Time, typeID, statusID int, betStatus string) ([]dataanalysis.MoneyStats, error) {
	session := repo.readDB.Table("accounting.accounting a")
	switch typeID {
	case
		10201,
		10203:
		session = session.Select("a.l80, SUM(CASE WHEN a.after > 0 THEN a.dealmoney + a.after ELSE a.after END) as total_money, count(1) as total_count, ? as type_id", typeID)
	default:
		session = session.Select("a.l80, SUM(a.dealmoney) as total_money ,count(1) as total_count, ? as type_id", typeID)
	}
	session = session.Joins("LEFT JOIN so_credit.world_cup_event_status b on a.eventid = b.game_id AND a.marketId = b.market_id AND a.selectionId = b.selection_id").
		Where(`
a.betStatus=? AND a.errorCode!=1
 AND a.opendate >= ?
 AND a.timestamp between ? AND ?
 AND IFNULL(b.status_id,0) = ?
`, betStatus, openDate, start, end, statusID)
	switch typeID {
	case 10303:
		session = session.Where("a.note!='保本活动'")
	case 10400:
		session = session.Where("a.note='保本活动'")
	}
	session = session.Group("a.l80")
	list := []dataanalysis.MoneyStats{}
	err := session.Find(&list).Error
	return list, err
}

func (repo *ReportRepo) Accounting(start, end, openDate time.Time, typeID int, betStatus string) ([]dataanalysis.MoneyStats, error) {
	session := repo.readDB.Table("accounting.accounting a")
	switch typeID {
	case
		10202:
		session = session.Select("a.l80,SUM(CASE WHEN a.after > 0 THEN a.dealmoney+a.after ELSE a.after END) AS total_money,count(1) as total_count, ? as type_id", typeID)
	default:
		session = session.Select("a.l80, SUM(a.dealmoney) as total_money, count(1) as total_count, ? as type_id", typeID)
	}
	session = session.Where(`
a.betStatus=? AND a.errorCode=1
 AND a.opendate >= ?
 AND a.timestamp between ? AND ?
`, betStatus, openDate, start, end).
		Group("a.l80")
	list := []dataanalysis.MoneyStats{}
	err := session.Find(&list).Error
	return list, err
}

func (repo *ReportRepo) MemberMoneyJoinUsers(start, end, openDate time.Time, typeID, action int, step dataanalysis.Step) ([]dataanalysis.MoneyStatsMemberMoney, error) {
	columnTime := "savedTime"
	columnStep := ",a.step"
	columnAction := ",a.action"
	session := repo.readDB.Table("so_credit.memberMoney a")
	session = session.Joins("JOIN so_credit.users b ON a.username = b.username")
	if action > 0 {
		columnAction = ""
		session = session.Where("a.action = ?", action)
	}
	switch step {
	case dataanalysis.StepLessOrEqual2:
		columnStep = ""
		columnTime = "requestTime"
		session = session.Where("a.step <= 2")
	default:
		session = session.Where("a.requestTime >= ?", openDate)
		switch step {
		case dataanalysis.StepEqual3:
			columnStep = ""
			session = session.Where("a.step = 3")
		case dataanalysis.StepGreaterOrEqual3:
			session = session.Where("a.step >= 3")
		}
	}

	session = session.Select(fmt.Sprintf("b.l80%s%s,SUM(requestAmount) as total_money, count(1) as total_count, ? as type_id", columnStep, columnAction), typeID)
	session = session.Where(fmt.Sprintf("a.%s between ? AND ?", columnTime), start, end)

	switch {
	case typeID <= 20299:
		// session = session.Where("a.saveBy not in (?)", []string{"后台", "后台-活动彩金", "后台-活动转点", "会员互转", "体验金", "负利红包", "翻本红包", "感恩回馈", "时来运转"})
		// todo 下面是gorm2.0的寫法
		session = session.Not(map[string]interface{}{"a.saveBy": []string{"后台", "后台-活动彩金", "后台-活动转点", "会员互转", "体验金", "负利红包", "翻本红包", "感恩回馈", "时来运转"}})
	case typeID >= 20300 && typeID <= 20499:
		session = session.Where("a.saveBy = ?", "后台")
	case typeID >= 20500 && typeID <= 20599:
		session = session.Where("a.saveBy = ?", "后台-活动彩金")
	case typeID >= 20600 && typeID <= 20699:
		session = session.Where("a.saveBy = ?", "后台-活动转点")
	case typeID >= 20700 && typeID <= 20799:
		session = session.Where("a.saveBy = ?", "体验金")
	case typeID >= 20800 && typeID <= 20899:
		session = session.Where("a.saveBy = ?", "会员互转")
	case typeID >= 20900 && typeID <= 20999:
		session = session.Where("a.saveBy = ?", "负利红包")
	}
	session = session.Group(fmt.Sprintf("b.l80%s%s", columnStep, columnAction))
	list := []dataanalysis.MoneyStatsMemberMoney{}
	err := session.Find(&list).Error
	return list, err
}
func (repo *ReportRepo) MemberRewardJoinUsers(start, end, update time.Time, typeID int) ([]dataanalysis.MoneyStats, error) {
	columnTime := "directpush_reward_update_time"
	columnSum := "directpush_reward"
	if typeID > 21100 {
		columnTime = "team_reward_update_time"
		columnSum = "team_reward"
	}
	session := repo.readDB.Table("tg_service.member_reward a").
		Select(fmt.Sprintf("b.l80, sum(a.%s) as total_money, COUNT(1) as total_count, ? as type_id", columnSum), typeID).
		Joins("JOIN so_credit.users b ON a.user_id = b.id").
		Where("update_time >= ?", update).
		Where(fmt.Sprintf("a.%s between ? AND ?", columnTime), start, end).
		Group("b.l80")
	list := []dataanalysis.MoneyStats{}
	err := session.Find(&list).Error
	return list, err
}

func (repo *ReportRepo) GetCompany() (map[int64]string, error) {
	session := repo.readDB.Table("so_credit.users a").
		Select("UPPER(LEFT(username, 1)) as name ,id").
		Where("`group`=80 and isDel=0")
	type Company struct {
		ID   int64
		Name string
	}
	list := []Company{}
	err := session.Find(&list).Error
	res := map[int64]string{}
	for _, v := range list {
		res[v.ID] = v.Name
	}

	return res, err
}

func (repo *ReportRepo) QueryList(start, end db.Timestamp) ([]dataanalysis.MoneyStatsQuery, error) {
	var result []dataanalysis.MoneyStatsQuery

	groupSum := repo.readDB.Table("dataanalysis.money_stats").
		Select(`l80, type_id, sum(total_count) as count, sum(total_money) as money`).
		Where("stats_time between ? AND ?", start, end).
		Group("l80, type_id")

	session := repo.readDB.Table("so_credit.users t1").
		Select("UPPER(LEFT(username, 1)) as company, t2.id as type_id , t2.money_flow, t2.action, ifnull(t3.count,0) as count, ifnull(t3.money,0) as money").
		Joins("left join dataanalysis.money_stats_type t2 on true").
		Joins("left join (?) AS t3 on t1.id=t3.l80 and t2.id=t3.type_id", groupSum).
		Where("t1.group=80 and t1.isDel=0 and ifnull(t2.action,'') !=''")

	err := session.
		Find(&result).Error
	return result, err
}
