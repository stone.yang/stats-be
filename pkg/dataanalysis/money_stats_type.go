package dataanalysis

// MoneyStatsTypeService by make truss
type MoneyStatsTypeService interface{}

// MoneyStatsTypeRepository by make truss
type MoneyStatsTypeRepository interface {
}

// MoneyStatsType by make truss
type MoneyStatsType struct {

	// 帳務類型分類
	Account string `json:"account" param:"account" form:"account" query:"account" json2:"account"`

	// 行為狀態分類
	Action string `json:"action" param:"action" form:"action" query:"action" json2:"action"`

	// 點數流向統計類型代碼
	ID int32 `json:"id" param:"id" form:"id" query:"id" json2:"id"`

	// 金流分類
	MoneyFlow string `json:"moneyFlow" param:"moneyFlow" form:"moneyFlow" query:"moneyFlow" json2:"money_flow"`
}

func (*MoneyStatsType) TableName() string {
	return "money_stats_type"
}
