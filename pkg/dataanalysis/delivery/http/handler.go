package http

import (
	"goapp/internal/pkg/db"
	middleware "goapp/internal/pkg/middleware"
	da "goapp/pkg/dataanalysis"
	"net/http"

	"github.com/labstack/echo/v4"
)

type Handler struct {
	reportService da.ReportService
}

func NewHandler(
	reportService da.ReportService,
) *Handler {
	return &Handler{
		reportService: reportService,
	}
}

// TG金流統計資料查詢
// @Summary 金流統計資料查詢
// @Description TG金流統計資料查詢測試
// @Tags root
// @Accept */*
// @Produce json
// @Param start query string true "開始時間" default(2021-03-26T13:00:00Z)
// @Param end query string true "結束時間" default(2021-03-31T23:59:59Z)
// @Success 200 {array} dataanalysis.MoneyStatsQuery
// @Router /query [get]
func (h *Handler) queryMoneyStatsEndpoint(c echo.Context) error {
	type reqType struct {
		Start db.Timestamp `query:"start"`
		End   db.Timestamp `query:"end"`
	}
	req := &reqType{}

	err := (&middleware.DefaultBinder{}).Bind(req, c)
	if err != nil {
		return nil
	}
	resp, err := h.reportService.QueryData(req.Start, req.End)
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, resp)
}

type importMoneyStatsReqType struct {
	StatsTime db.Timestamp `json:"statsTime" swaggertype:"primitive,string" example:"2021-04-07T12:00:00Z"`
}

// TG金流統計資料重匯
// @Summary 金流統計資料重匯
// @Description TG金流統計資料重匯測試
// @Tags root
// @Accept json
// @Produce json
// @Param statsTime body importMoneyStatsReqType true "統計時間"
// @Success 200 {object} interface{}
// @Router /import [put]
func (h *Handler) importMoneyStatsEndpoint(c echo.Context) error {
	req := &importMoneyStatsReqType{}
	err := (&middleware.DefaultBinder{}).Bind(req, c)
	if err != nil {
		return nil
	}
	err = h.reportService.ImportData(req.StatsTime)
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, nil)
}
