package dataanalysis

import (
	"goapp/internal/pkg/db"
	"time"
)

type ReportRepository interface {
	GetSumData(start, end time.Time) (*MoneyStats, error)
	BetJoinWorldCupEventStatus(start, end time.Time, typeID, statusID int, betStatus string) ([]MoneyStats, error)
	Bet(start, end time.Time, typeID int) ([]MoneyStats, error)
	AccountingJoinWorldCupEventStatus(start, end, openDate time.Time, typeID, statusID int, betStatus string) ([]MoneyStats, error)
	Accounting(start, end, openDate time.Time, typeID int, betStatus string) ([]MoneyStats, error)
	MemberMoneyJoinUsers(start, end, openDate time.Time, typeID, action int, step Step) ([]MoneyStatsMemberMoney, error)
	MemberRewardJoinUsers(start, end, update time.Time, typeID int) ([]MoneyStats, error)
	GetCompany() (map[int64]string, error)
	QueryList(start, end db.Timestamp) ([]MoneyStatsQuery, error)
}

type ReportService interface {
	Sum0() error
	SyncDataByTime(currentHHTime time.Time) error
	SyncData() error
	ImportData(statsTime db.Timestamp) error
	QueryData(startTime, endTime db.Timestamp) ([]MoneyStatsQuery, error)
}
