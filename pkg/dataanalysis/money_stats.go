package dataanalysis

import (
	"goapp/internal/pkg/db"

	"github.com/shopspring/decimal"
	"gorm.io/gorm"
)

// MoneyStatsService by make truss
type MoneyStatsService interface{}

// MoneyStatsRepository by make truss
type MoneyStatsRepository interface {
	Create(*MoneyStats) error
	CreateList([]MoneyStats) error
	TypeList(notIn []int32) ([]MoneyStatsType, error)
	DeleteList(statsTime db.Timestamp)
	QueryList(start, end db.Timestamp) ([]MoneyStats, error)
}

var StatsTime db.Timestamp

// MoneyStats by make truss
type MoneyStats struct {

	// 統計資料建立時間
	CreatedAt db.Timestamp `json:"createdAt" param:"createdAt" form:"createdAt" query:"createdAt" json2:"created_at"`

	// 公司
	L80 int64 `json:"l80" param:"l80" form:"l80" query:"l80" json2:"l80"`

	// 統計時間
	StatsTime db.Timestamp `json:"statsTime" param:"statsTime" form:"statsTime" query:"statsTime" json2:"stats_time"`

	// 統計筆數
	TotalCount int32 `json:"totalCount" param:"totalCount" form:"totalCount" query:"totalCount" json2:"total_count"`

	// 統計金額
	TotalMoney decimal.Decimal `json:"totalMoney" param:"totalMoney" form:"totalMoney" query:"totalMoney" json2:"total_money"`

	// 參考money_stats_type.id
	TypeID int32 `json:"typeID" param:"typeID" form:"typeID" query:"typeID" json2:"type_id"`
}

func (*MoneyStats) TableName() string {
	return "money_stats"
}

func (m *MoneyStats) AfterFind(tx *gorm.DB) (err error) {
	m.CreatedAt = db.Timestamp{Time: tx.NowFunc()}
	m.StatsTime = StatsTime
	return
}

type MoneyStatsMemberMoney struct {
	MoneyStats
	Step   int32 `json:"step" param:"step" form:"step" query:"step" json2:"step"`
	Action int32 `json:"action" param:"action" form:"action" query:"action" json2:"action"`
}

func (m *MoneyStatsMemberMoney) AfterFind(tx *gorm.DB) (err error) {
	m.MoneyStats.AfterFind(tx)
	switch m.TypeID {
	case
		20100,
		20200,
		20300,
		20400,
		20500,
		20600:
		switch m.Step {
		case 3:
			m.TypeID += 2
		case 4:
			m.TypeID += 3
		}
	case
		20700,
		20800:
		switch m.Action {
		case
			1,
			2:
			m.TypeID += m.Action
		}
	}
	return
}
