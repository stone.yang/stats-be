package service

import (
	"goapp/internal/pkg/db"
	"goapp/internal/pkg/timeutil"
	da "goapp/pkg/dataanalysis"
	"time"

	linq "github.com/ahmetb/go-linq/v3"

	log "github.com/sirupsen/logrus"
)

type ReportService struct {
	moneyStatsRepo da.MoneyStatsRepository
	reportRepo     da.ReportRepository
}

func NewReportService(
	moneyStatsRepo da.MoneyStatsRepository,
	reportRepo da.ReportRepository,
) da.ReportService {
	return &ReportService{
		moneyStatsRepo: moneyStatsRepo,
		reportRepo:     reportRepo,
	}
}

func (svc *ReportService) Sum0() error {
	notIn := []int32{
		10000,
		10100,
		10200,
		10300,
		// 10400,
		20000,
		20100,
		20200,
		20300,
		20400,
		20500,
		20600,
		20700,
		20800,
		20900,
		21000,
		21100,
	}
	list, err := svc.moneyStatsRepo.TypeList(notIn)
	if err != nil {
		return err
	}
	log.Info(list)
	return nil
}

func (svc *ReportService) SyncDataByTime(currentHHTime time.Time) error {
	startTime := currentHHTime.Add(time.Minute * 10 * -1)
	endTime := currentHHTime.Add(time.Millisecond * -1)

	openDate, _ := timeutil.StringParseTime(currentHHTime.AddDate(0, 0, -1).Format("2006-01-02 00:00:00"))
	updateTime, _ := timeutil.StringParseTime(currentHHTime.AddDate(0, 0, -2).Format("2006-01-02 00:00:00"))

	da.StatsTime = db.Timestamp{Time: startTime}
	startYesterday := startTime.AddDate(0, 0, -1)
	endYesterday := endTime.AddDate(0, 0, -1)
	importMoneyStats := func(list []da.MoneyStats, err error) error {
		if err != nil {
			return err
		}
		err = svc.moneyStatsRepo.CreateList(list)
		if err != nil {
			log.Panicf("moneyStatsRepo.CreateList: %v", err)
			svc.moneyStatsRepo.DeleteList(da.StatsTime)
		}
		return err
	}
	importMoneyStatsMemberMoney := func(list []da.MoneyStatsMemberMoney, err error) error {
		if err != nil {
			return err
		}
		moneyStatsList := []da.MoneyStats{}
		linq.From(list).SelectT(func(m da.MoneyStatsMemberMoney) da.MoneyStats {
			return m.MoneyStats
		}).ToSlice(&moneyStatsList)
		return importMoneyStats(moneyStatsList, nil)
	}
	//import data
	{
		svc.moneyStatsRepo.DeleteList(da.StatsTime)
		var err error
		if err = importMoneyStats(svc.reportRepo.BetJoinWorldCupEventStatus(startTime, endTime, 10101, 0, "M")); err != nil {
			return err
		}
		if err = importMoneyStats(svc.reportRepo.BetJoinWorldCupEventStatus(startTime, endTime, 10103, 1, "M")); err != nil {
			return err
		}
		if err = importMoneyStats(svc.reportRepo.Bet(startTime, endTime, 10102)); err != nil {
			return err
		}
		if err = importMoneyStats(svc.reportRepo.AccountingJoinWorldCupEventStatus(startTime, endTime, openDate, 10201, 0, "S")); err != nil {
			return err
		}
		if err = importMoneyStats(svc.reportRepo.AccountingJoinWorldCupEventStatus(startTime, endTime, openDate, 10203, 1, "S")); err != nil {
			return err
		}
		if err = importMoneyStats(svc.reportRepo.AccountingJoinWorldCupEventStatus(startTime, endTime, openDate, 10301, 0, "C")); err != nil {
			return err
		}
		if err = importMoneyStats(svc.reportRepo.AccountingJoinWorldCupEventStatus(startTime, endTime, openDate, 10303, 1, "C")); err != nil {
			return err
		}
		if err = importMoneyStats(svc.reportRepo.AccountingJoinWorldCupEventStatus(startTime, endTime, openDate, 10400, 1, "C")); err != nil {
			return err
		}
		if err = importMoneyStats(svc.reportRepo.Accounting(startTime, endTime, openDate, 10202, "S")); err != nil {
			return err
		}
		if err = importMoneyStats(svc.reportRepo.Accounting(startTime, endTime, openDate, 10302, "C")); err != nil {
			return err
		}
		if err = importMoneyStatsMemberMoney(svc.reportRepo.MemberMoneyJoinUsers(startTime, endTime, openDate, 20101, 1, da.StepLessOrEqual2)); err != nil {
			return err
		}
		//20102, 20103
		if err = importMoneyStatsMemberMoney(svc.reportRepo.MemberMoneyJoinUsers(startTime, endTime, openDate, 20100, 1, da.StepGreaterOrEqual3)); err != nil {
			return err
		}
		if err = importMoneyStatsMemberMoney(svc.reportRepo.MemberMoneyJoinUsers(startTime, endTime, openDate, 20201, 2, da.StepLessOrEqual2)); err != nil {
			return err
		}
		//20202, 20203
		if err = importMoneyStatsMemberMoney(svc.reportRepo.MemberMoneyJoinUsers(startTime, endTime, openDate, 20200, 2, da.StepGreaterOrEqual3)); err != nil {
			return err
		}
		if err = importMoneyStatsMemberMoney(svc.reportRepo.MemberMoneyJoinUsers(startTime, endTime, openDate, 20301, 1, da.StepLessOrEqual2)); err != nil {
			return err
		}
		//20302, 20303
		if err = importMoneyStatsMemberMoney(svc.reportRepo.MemberMoneyJoinUsers(startTime, endTime, openDate, 20300, 1, da.StepGreaterOrEqual3)); err != nil {
			return err
		}
		if err = importMoneyStatsMemberMoney(svc.reportRepo.MemberMoneyJoinUsers(startTime, endTime, openDate, 20401, 2, da.StepLessOrEqual2)); err != nil {
			return err
		}
		//20402, 20403
		if err = importMoneyStatsMemberMoney(svc.reportRepo.MemberMoneyJoinUsers(startTime, endTime, openDate, 20400, 2, da.StepGreaterOrEqual3)); err != nil {
			return err
		}
		//20502, 20503
		if err = importMoneyStatsMemberMoney(svc.reportRepo.MemberMoneyJoinUsers(startTime, endTime, openDate, 20500, 1, da.StepGreaterOrEqual3)); err != nil {
			return err
		}
		//20602, 20603
		if err = importMoneyStatsMemberMoney(svc.reportRepo.MemberMoneyJoinUsers(startTime, endTime, openDate, 20600, 1, da.StepGreaterOrEqual3)); err != nil {
			return err
		}
		//20701, 20702
		if err = importMoneyStatsMemberMoney(svc.reportRepo.MemberMoneyJoinUsers(startTime, endTime, openDate, 20700, 0, da.StepEqual3)); err != nil {
			return err
		}
		//20801, 20802
		if err = importMoneyStatsMemberMoney(svc.reportRepo.MemberMoneyJoinUsers(startTime, endTime, openDate, 20800, 0, da.StepEqual3)); err != nil {
			return err
		}
		if err = importMoneyStatsMemberMoney(svc.reportRepo.MemberMoneyJoinUsers(startTime, endTime, openDate, 20901, 1, da.StepEqual3)); err != nil {
			return err
		}
		if err = importMoneyStats(svc.reportRepo.MemberRewardJoinUsers(startTime, endTime, updateTime, 21001)); err != nil {
			return err
		}
		if err = importMoneyStats(svc.reportRepo.MemberRewardJoinUsers(startYesterday, endYesterday, updateTime, 21002)); err != nil {
			return err
		}
		if err = importMoneyStats(svc.reportRepo.MemberRewardJoinUsers(startTime, endTime, updateTime, 21101)); err != nil {
			return err
		}
		if err = importMoneyStats(svc.reportRepo.MemberRewardJoinUsers(startYesterday, endYesterday, updateTime, 21102)); err != nil {
			return err
		}
	}
	return nil
}

func (svc *ReportService) SyncData() error {
	currentHHTime, _ := timeutil.StringParseTime(db.NowFunc().Format("2006-01-02 15:04:00"))
	duration := time.Duration(currentHHTime.Minute() % 10)
	currentHHTime = currentHHTime.Add(time.Minute * duration * -1)
	return svc.SyncDataByTime(currentHHTime)
}

func (svc *ReportService) ImportData(statsTime db.Timestamp) error {
	hhTime, _ := timeutil.StringParseTime(statsTime.Time.Add(time.Minute * 1).Format("2006-01-02 15:04:00"))
	return svc.SyncDataByTime(hhTime)
}

func (svc *ReportService) QueryData(startTime, endTime db.Timestamp) ([]da.MoneyStatsQuery, error) {
	//format := "2006-01-02 15:04:05"
	//startTime, _ := time.ParseInLocation(format, "2021-03-28 00:00:00", time.UTC)
	//endTime, _ := time.ParseInLocation(format, "2021-03-29 00:00:00", time.UTC)
	list, err := svc.reportRepo.QueryList(startTime, endTime)
	if err != nil {
		return nil, err
	}
	return list, nil
}
