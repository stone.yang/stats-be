package service

import (
	"goapp/pkg/stats"
)

type TestTable2Service struct {
	repo stats.TestTable2Repository
}

func NewTestTable2Service(repo stats.TestTable2Repository) stats.TestTable2Service {
	return &TestTable2Service{
		repo: repo,
	}
}

func (svc *TestTable2Service) Create(model *stats.TestTable2) error {
	return svc.repo.Create(model)
}
