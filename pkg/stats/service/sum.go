package service

import (
	"goapp/internal/pkg/db"
	"goapp/internal/pkg/serial"
	"goapp/pkg/stats"
	"time"

	log "github.com/sirupsen/logrus"
)

type SumService struct {
	testTable1Repo stats.TestTable1Repository
	testTable2Repo stats.TestTable2Repository
}

func NewSumService(
	testTable1Repo stats.TestTable1Repository,
	testTable2Repo stats.TestTable2Repository,
) stats.SumService {
	return &SumService{
		testTable1Repo: testTable1Repo,
		testTable2Repo: testTable2Repo,
	}
}

func (svc *SumService) Sum0(start, end time.Time) error {
	customTestTable1, err := svc.testTable1Repo.GetSumData(start, end)
	if err != nil {
		return err
	}
	result := &stats.TestTable2{}
	result.Total1 = customTestTable1.Total1
	result.Total2 = customTestTable1.Total2
	result.ID = serial.GenerateID()
	result.DateTime = db.Timestamp{Time: time.Now()}
	return svc.testTable2Repo.Create(result)
}

func (svc *SumService) Sum1(start, end time.Time) error {
	list, err := svc.testTable1Repo.ListByTime(start, end)
	if err != nil {
		return err
	}
	result := &stats.TestTable2{}
	for _, v := range list {
		result.Total1 += v.Money1
		result.Total2 = result.Total2.Add(v.Money2)
		//result.Total2 += v.Money2
	}
	result.ID = serial.GenerateID()
	result.DateTime = db.Timestamp{Time: time.Now()}
	return svc.testTable2Repo.Create(result)
}
func (svc *SumService) Sum2(start, end time.Time) error {
	list, err := svc.testTable1Repo.JoinAnotherDbTable(start, end)
	if err != nil {
		return err
	}
	log.Info(list[0])
	return nil
}
