package stats

import (
	"time"

	"goapp/internal/pkg/db"

	"github.com/shopspring/decimal"
	"gorm.io/gorm"
)

type CustomTestTable1 struct {
	Total1 int
	Total2 decimal.Decimal
}

type TestTable1 struct {
	ID       int             `gorm:"column:id;primary_key;AUTO_INCREMENT" json:"id"`
	Money1   int             `gorm:"column:money1" json:"money1" description:"金額1"`
	Money2   decimal.Decimal `gorm:"column:money2" json:"money2" description:"金額1"`
	DateTime db.Timestamp    `gorm:"column:date_time" json:"-" description:"資料時間"`
}

func (*TestTable1) TableName() string {
	return "test_table1"
}

func (u *TestTable1) AfterFind(tx *gorm.DB) (err error) {
	if u.Money1 < 300 {
		u.Money1 = 1000
	}
	return
}

type TestTable1Repository interface {
	WriteDB() *gorm.DB
	GetSumData(start, end time.Time) (*CustomTestTable1, error)
	ListByTime(start, end time.Time) ([]TestTable1, error)
	JoinAnotherDbTable(start, end time.Time) ([]TestTable1, error)
}
