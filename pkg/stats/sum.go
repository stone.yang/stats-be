package stats

import "time"

type SumService interface {
	Sum0(start, end time.Time) error
	Sum1(start, end time.Time) error
	Sum2(start, end time.Time) error
}
