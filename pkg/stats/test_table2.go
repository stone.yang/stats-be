package stats

import (
	"goapp/internal/pkg/db"

	"gorm.io/gorm"

	"github.com/shopspring/decimal"
)

type TestTable2 struct {
	ID       string          `gorm:"column:id;primary_key" json:"id"`
	Total1   int             `gorm:"column:total1" json:"total1" description:"總額1"`
	Total2   decimal.Decimal `gorm:"column:total2" json:"total2" description:"總額2"`
	DateTime db.Timestamp    `gorm:"column:date_time" json:"-" description:"資料時間"`
}

func (*TestTable2) TableName() string {
	return "test_table2"
}

type TestTable2Repository interface {
	WriteDB() *gorm.DB
	Create(*TestTable2) error
}

type TestTable2Service interface {
	Create(*TestTable2) error
}
