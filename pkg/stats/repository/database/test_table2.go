package database

import (
	"goapp/pkg/stats"

	"gorm.io/gorm"
)

// NewTestTable1Repository new repository func
func NewTestTable2Repository(readDB, writeDB *gorm.DB) stats.TestTable2Repository {
	return &TestTable2Repo{
		readDB,
		writeDB,
	}
}

type TestTable2Repo struct {
	readDB  *gorm.DB
	writeDB *gorm.DB
}

func (repo *TestTable2Repo) WriteDB() *gorm.DB {
	return repo.writeDB
}

func (repo *TestTable2Repo) Create(model *stats.TestTable2) error {
	return repo.writeDB.Create(model).Error
}
